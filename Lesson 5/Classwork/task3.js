/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }

*/

function Dog(name, kind) {
    this.name = name;
    this.kind = kind;
    this.runs = function(){
        console.log('Cобака '+ this.name + ' бежит');
    }
    this.eats = function(){
        console.log('Собака '+ this.name + ' есть');
    }
    this.props = function(){
        for(key in this){
    console.log(key, this[key]);
}
    }
}

let firstDog = new Dog("Vitalic", "пикинес");
console.log(firstDog);
firstDog.eats();
firstDog.runs();
firstDog.props();



