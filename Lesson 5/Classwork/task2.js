/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/

  h1 = document.createElement('h1');
  h1.innerText='I know how binding works in JS';

  let colors = {
    color: 'red',
    background: 'yellow',    
    h1: h1
  }

  
  // function myCall( back ){
  //   document.body.style.color = back;
  //   document.body.style.background = this.background;
  //   document.body.appendChild(this.h1);
    
  // }
  // myCall.call(colors, 'purple');
  

  function myBind(){
    document.body.style.color = this.color;
    document.body.style.background = this.background;
    document.body.appendChild(this.h1);
     
    }
   let binded = myBind.bind(colors);
   binded()
   

  // function myApply(color, back){
  //   document.body.style.color = color;
  //   document.body.style.background = back;
  //   document.body.appendChild(this.h1);    
  // }

  // let x = ['green', 'red'];

  // myApply.apply(colors, x);
