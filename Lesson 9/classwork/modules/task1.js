/*

  Задание:
    Написать конструктор обьекта. Отдельные функции разбить на модули
    и использовать внутри самого конструктора.
    Каждую функцию выделить в отдельный модуль и собрать.

    Тематика - птицы.
    Птицы могут:
      - Нестись
      - Летать
      - Плавать
      - Кушать
      - Охотиться
      - Петь
      - Переносить почту
      - Бегать

  Составить птиц (пару на выбор, не обязательно всех):
      Страус
      Голубь
      Курица
      Пингвин
      Чайка
      Ястреб
      Сова


 */

 //  class Bird {
 //    constructor(name, speed, latitude, sound){
 //      this.name = name;
 //      this.speed = speed;
 //      this.latitude = latitude;
 //      this.sound = sound;
 //    }
 //    fly(latitude){
 //      this.latitude += latitude;
 //      console.log(`Bird ${this.name} is flying on ${this.latitude} metres`)
 //    }
 //    sing(sound){
 //      this.sound = "Chick Chick";
 //      console.log(`Bird ${this.name}: "${this.sound}" `)
 //    }
 //    run(speed){
 //      this.speed += speed;
 //      console.log(`Bird ${this.name} is running with ${this.speed} km/h`)
 //    }
 //  }

 // let Sparrow = new Bird("Sparrow", 0, 0, "none")
 // console.log(Sparrow);
 // console.log(Sparrow.fly(20));
 // console.log(Sparrow.sing());
 // console.log(Sparrow.run(10));

 // let Jackdow = new Bird ("Jackdow", 0, 0, "none")
 // console.log(Jackdow);
 // console.log(Jackdow.fly(45));
 // console.log(Jackdow.sing());
 // console.log(Jackdow.run(23));
import  './a.js'
 console.log('./task1.js')